from django.conf.urls import url, include
from django.contrib import admin
urlpatterns = (
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^web/', include('rich_html.urls')),
)
