from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^(?P<category>[^\/]+)/(?P<title>[^\/]+)$', views.rich_html, name='rich_html'),
]
