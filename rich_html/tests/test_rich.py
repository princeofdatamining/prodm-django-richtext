from django.test import TestCase, Client

from .. import models

class TestRich(TestCase):

    def force_rich(self, category, title):
        return models.Richtexts.get_or_create(defaults=dict(
            content='''
                <h1>{title}</h1>
                <h2>{title}</h2>
                <h3>{title}</h3>
            '''.format(title=title)
        ), category=category, title=title)

    def setUp(self):
        self.h1, _ = self.force_rich('', 'root')
        self.h2, _ = self.force_rich('help', 'guide')

    def test(self):
        u1 = self.h1.url
        u2 = self.h2.url
        self.assertIn('/_/', u1)
        #
        resp = Client().get(u2.replace('guide', 'blah'))
        self.assertEqual(404, resp.status_code)
        #
        resp = Client().get(u1)
        self.assertEqual(200, resp.status_code)
        self.assertContains(resp, self.h1.title, 3)
        #
        resp = Client().get(u2)
        self.assertEqual(200, resp.status_code)
        self.assertContains(resp, self.h2.title, 3)
