from django.shortcuts import render, get_object_or_404, Http404
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

from . import models

def rich_html(request, **kwargs):
    if kwargs.get('category') == '_':
        kwargs['category'] = ''
    try:
        richtext = models.Richtexts.get(**kwargs)
    except models.ObjectDoesNotExist:
        raise Http404
    return HttpResponse(richtext.content)
