from django.contrib import admin, messages
from django import forms
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

from . import models, constant

@admin.register(models.Richtext)
class RichtextAdmin(admin.ModelAdmin):

    list_display = ['title', 'href', 'visible']
    fields = ('title', 'content', 'create_time', 'visible')
    list_filter = ('visible',)

    CATEGORY = ''

    def get_queryset(self, *args, **kwargs):
        qs = super(RichtextAdmin, self).get_queryset(*args, **kwargs)
        if self.CATEGORY:
            qs = qs.filter(category=self.CATEGORY)
        return qs

    def save_model(self, request, obj, form, change):
        if not change:
            obj.category = self.CATEGORY
        return super(RichtextAdmin, self).save_model(request, obj, form, change)
