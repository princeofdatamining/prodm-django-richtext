from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

from . import constant

class RichtextManager(models.Manager):

    def public(self, *args, queryset=None, **kwargs):
        return (self if queryset is None else queryset).filter(*args, visible=True, **kwargs)

Richtexts = RichtextManager()

class Richtext(models.Model):

    class Meta:
        verbose_name = constant.MODEL_RICHTEXT
        verbose_name_plural = constant.MODEL_RICHTEXTS
        unique_together = ['category', 'title']
        ordering = ['-visible', '-create_time']

    objects = Richtexts
    
    category = models.CharField(constant.CATEGORY, max_length=32, blank=True, default='')
    title = models.CharField(constant.TITLE, max_length=191)
    content = RichTextUploadingField(config_name='default', verbose_name=constant.RICH)
    create_time = models.DateTimeField(constant.TIME_CREATE, null=True, blank=True, default=timezone.now)
    visible = models.BooleanField(constant.VISIBLE, default=True)

    def __str__(self):
        return self.title

    @property
    def url(self):
        return reverse('rich_html', kwargs=dict(
            category=self.category or '_',
            title=self.title,
        ))

    def href(self):
        return mark_safe('<a href="{}" target="_blank">{}</a>'.format(
            self.url, constant.PREVIEW
        ))
    href.short_description = constant.HREF
