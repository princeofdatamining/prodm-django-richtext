from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = pgettext_lazy('APP', 'rich_html')

MODEL_RICHTEXT = pgettext_lazy('Model', 'Richtext')
MODEL_RICHTEXTS = pgettext_lazy('Model', 'Richtexts')

CATEGORY = pgettext_lazy('Richtext', 'category')
TITLE = pgettext_lazy('Richtext', 'title')
RICH = pgettext_lazy('Richtext', 'content')
TIME_CREATE = pgettext_lazy('Richtext', 'create time')
VISIBLE = pgettext_lazy('Richtext', 'visible')
HREF = pgettext_lazy('Richtext', 'link')
PREVIEW = pgettext_lazy('Richtext', 'Preview')
